import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Time {
    int hour;
    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    int minute;
    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    int second;
    
    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public Time() {
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    public void setTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;

    }
    
     public String toString() {
        String timeColonPattern = "HH:mm:ss";
        DateTimeFormatter timeColonFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
        LocalTime colonTime = LocalTime.of(this.hour, this.minute, this.second);
        return timeColonFormatter.format(colonTime);
        }
    public Time nextSecond() {
        Time newTime = new Time();
        second +=1;
        return newTime;
    }

    public Time previousSecond() {
        Time newTime = new Time();
        second -=1;
        return newTime;
    }

    }



