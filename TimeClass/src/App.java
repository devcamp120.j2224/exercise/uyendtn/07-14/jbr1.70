public class App {
    public static void main(String[] args) throws Exception {
       Time time1 = new Time(5, 6, 9);
       Time time2 = new Time(10, 20, 30);

       System.out.println(time1.toString());
       System.out.println(time2.toString());

       //time1 thêm 1 giây và time2 giảm 1 giây
       time1.nextSecond();
       System.out.println(time1.toString());

       time2.previousSecond();
       System.out.println(time2.toString());

    }
}
